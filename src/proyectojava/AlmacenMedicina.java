
package proyectojava;

public class AlmacenMedicina {

   Medicina[] medicinas;
    
    public AlmacenMedicina(int nroMedicinas) {
        medicinas = new Medicina[nroMedicinas];
    }
     
    public class Medicina{
        int idMedicina;
        String nombre;
        String fVencimiento;

        public Medicina(int idMedicina, String nombre, String fVencimiento) {
            this.idMedicina = idMedicina;
            this.nombre = nombre;
            this.fVencimiento = fVencimiento;
        }

        public String getfVencimiento() {
            return fVencimiento;
        }

        public void setfVencimiento(String fVencimiento) {
            this.fVencimiento = fVencimiento;
        }

        public int getIdMedicina() {
            return idMedicina;
        }

        public void setIdMedicina(int idMedicina) {
            this.idMedicina = idMedicina;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
        
    }
}
