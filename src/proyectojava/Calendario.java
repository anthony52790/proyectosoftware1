
package proyectojava;

public class Calendario {

    CitaMedica[] citas;
    
    public Calendario(int nroCitas) {
        citas= new CitaMedica[nroCitas];
    }
    
    public class CitaMedica{
      
      Paciente paciente;
      Medico medico;
        
      public int idCita;
      public String fCita;

        public CitaMedica(int idCita, String fCita) {
            this.idCita = idCita;
            this.fCita = fCita;
        }
        
        public String getfCita() {
            return fCita;
        }

        public void setfCita(String fCita) {
            this.fCita = fCita;
        }

        public int getIdCita() {
            return idCita;
        }

        public void setIdCita(int idCita) {
            this.idCita = idCita;
        }

        public Medico getMedico() {
            return medico;
        }

        public void setMedico(Medico medico) {
            this.medico = medico;
        }

        public Paciente getPaciente() {
            return paciente;
        }

        public void setPaciente(Paciente paciente) {
            this.paciente = paciente;
        }
    }
}
