
package proyectojava;

import proyectojava.AlmacenMedicina.Medicina;

public class HistorialClinico {
    
    ConsultaMedica[] consultas;
    
    public class ConsultaMedica {
    
        int idCodigo;
        String diagnostico;

        Enfermedad enfermedad;
        AlmacenMedicina.Medicina medicina;
        
        public ConsultaMedica(int idCodigo, String diagnostico) {
            this.idCodigo = idCodigo;
            this.diagnostico = diagnostico;
        }

        public String getDiagnostico() {
            return diagnostico;
        }

        public void setDiagnostico(String diagnostico) {
            this.diagnostico = diagnostico;
        }

        public Enfermedad getEnfermedad() {
            return enfermedad;
        }

        public void setEnfermedad(Enfermedad enfermedad) {
            this.enfermedad = enfermedad;
        }

        public int getIdCodigo() {
            return idCodigo;
        }

        public void setIdCodigo(int idCodigo) {
            this.idCodigo = idCodigo;
        }

        public Medicina getMedicina() {
            return medicina;
        }

        public void setMedicina(Medicina medicina) {
            this.medicina = medicina;
        }
        
    }
}
