package proyectojava;

public class Paciente extends Persona{

  public int idPaciente;
  public int nroRegistro;
  public String fNacimiento;

  Calendario.CitaMedica cita;
  
    public Paciente(int idPaciente, int nroRegistro, String fNacimiento, String nombre, String apellidos, String ciudad, String direccion, String sexo, int telefono) {
        super(nombre, apellidos, ciudad, direccion, sexo, telefono);
        this.idPaciente = idPaciente;
        this.nroRegistro = nroRegistro;
        this.fNacimiento = fNacimiento;
    }

    public String getfNacimiento() {
        return fNacimiento;
    }

    public void setfNacimiento(String fNacimiento) {
        this.fNacimiento = fNacimiento;
    }

    public int getIdPaciente() {
        
        return idPaciente;
    }

    public void setIdPaciente(int idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getNroRegistro() {
        return nroRegistro;
    }

    public void setNroRegistro(int nroRegistro) {
        this.nroRegistro = nroRegistro;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
  
}
