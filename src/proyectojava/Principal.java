
package proyectojava;

import org.jvnet.substance.*;
import org.jvnet.substance.watermark.*;
import javax.swing.JFrame;
import Ventanas.Presentacion;

public class Principal extends Presentacion {

    public static void main(String[] args) {
        
       JFrame.setDefaultLookAndFeelDecorated(true);
       SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.ModerateSkin");
       SubstanceLookAndFeel.setCurrentTheme("org.jvnet.substance.theme.SubstanceAquaTheme"); 
       SubstanceLookAndFeel.setCurrentWatermark("org.jvnet.substance.watermark.SubstanceBinaryWatermark");

       Presentacion p = new Presentacion();
       p.setVisible(true);
       p.setTitle("Recepcion Citas Medicas");
       //p.setResizable(false);
       p.setLocationRelativeTo(null);
    }
}
